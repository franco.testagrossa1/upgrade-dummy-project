module Script.Common where

import Daml.Script

import DA.Text                    (unwords)

import qualified DA.Exception as Exception
import qualified DA.List as List
import qualified DA.Either as Either
import DA.Action (unless, when)

data Input = Input with
  operator: Party
  rollback: Bool

newtype MigrationError = MigrationError { reason : Text }

type MigrationResult a = Either MigrationError a

withErrorHandling : (Exception.ActionCatch m) => m i -> m (MigrationResult i)
withErrorHandling action = 
  try do
    Right <$> action
  catch (ex : AnyException) -> do
        let errorMessage = Exception.message ex
        pure $ Left (MigrationError errorMessage)

migrateDirectly : forall i i' v. (Template i, HasInterfaceView i v) =>
  Text -> 
  Party -> 
  (Party -> [ContractId i] -> Script [MigrationResult (ContractId i')]) -> 
  Script [MigrationResult (ContractId i')]
migrateDirectly contractType party f = script do
  cidListResult <- withErrorHandling $ do 
          cidList <- map fst <$> queryInterface @i party
          pure cidList
  
  (migratedList, cidList) <- case cidListResult of
      Left err -> pure ([Left err], [])
      Right cidList -> (\r -> (r, cidList)) <$> (f party cidList)

  let (errors, success) = Either.partitionEithers migratedList

  unless (null errors) $ do
    debug ("[ERROR] errors reason: " <> show (reason $ List.head errors))
    debug ("[ERROR] errors count: " <> show (length errors))

  unless (null success) $ do
    let originCount = length cidList
        migratedCount = length success
    info contractType originCount migratedCount

  when (null success) $ warn contractType
    
  pure migratedList
  
  where
    info contractType count count' =
      debug $ unwords ["[INFO] Migrated", contractType, "contracts: from", show count, "to", show count']
    
    warn contractType =
      debug $ unwords ["[WARN] No contracts migrated for", contractType]

preMigrateDirectly : forall a b. (Template a, HasAgreement a) =>
  Party ->
  ([(ContractId a, a)] -> Script [MigrationResult b]) ->
  Script [MigrationResult b]
preMigrateDirectly party f = script do
  templates <- withErrorHandling $ query @a party
  (results, _) <- case templates of
      Left err -> pure ([Left err], [])
      Right ts -> (\r -> (r, ts)) <$> (f ts)
  pure results